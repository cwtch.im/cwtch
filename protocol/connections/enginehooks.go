package connections

import "git.openprivacy.ca/cwtch.im/tapir"

type EngineHooks interface {
	SendPeerMessage(connection tapir.Connection, message []byte) error
}

type DefaultEngineHooks struct {
}

func (deh DefaultEngineHooks) SendPeerMessage(connection tapir.Connection, message []byte) error {
	return connection.Send(message)
}
