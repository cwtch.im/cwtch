package app

// DefactoPasswordForUnencryptedProfiles is used to offer "un-passworded" profiles. Our storage encrypts everything with a password. We need an agreed upon
// password to use in that case, that the app case use behind the scenes to password and unlock with
// See also: https://docs.cwtch.im/security/components/cwtch/profile-encryption
const DefactoPasswordForUnencryptedProfiles = "be gay do crime"
