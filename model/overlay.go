package model

import (
	"time"
)

// MessageWrapper is the canonical Cwtch overlay wrapper
type MessageWrapper struct {
	Overlay int    `json:"o"`
	Data    string `json:"d"`

	// when the data was assembled
	SendTime *time.Time `json:"s,omitempty"`

	// when the data was transmitted (by protocol engine e.g. over Tor)
	TransitTime *time.Time `json:"t,omitempty"`

	// when the data was received
	RecvTime *time.Time `json:"r,omitempty"`
}

// Channel is defined as being the last 3 bits of the overlay id
// Channel 0 is reserved for the main conversation
// Channel 2 is reserved for conversation admin (managed groups)
// Channel 7 is reserved for streams (no ack, no store)
func (mw MessageWrapper) Channel() int {
	// 1024 / 0x400 is the start of new channel overlays
	if mw.Overlay > 1024 {
		return mw.Overlay & 0x07
	}
	// for backward compatibilty all overlays less than 0x400 i.e. 1024 are
	// mapped to channel 0 regardless of their channel status.
	return 0
}

// If Overlay is a Stream Message it should not be ackd, or stored.
func (mw MessageWrapper) IsStream() bool {
	return mw.Channel() == 0x07
}

// OverlayChat is the canonical identifier for chat overlays
const OverlayChat = 1

// OverlayInviteContact is the canonical identifier for the contact invite overlay
const OverlayInviteContact = 100

// OverlayInviteGroup is the canonical identifier for the group invite overlay
const OverlayInviteGroup = 101

// OverlayFileSharing is the canonical identifier for the file sharing overlay
const OverlayFileSharing = 200

// ManageGroupEvent is the canonical identifier for the manage group overlay
const OverlayManageGroupEvent = 0x402
