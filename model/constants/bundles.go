package constants

// ServerPrefix precedes a server import statement
const ServerPrefix = "server:"

// TofuBundlePrefix precedes a server and a group import statement
const TofuBundlePrefix = "tofubundle:"

// GroupPrefix precedes a group import statement
const GroupPrefix = "torv3"

// ImportBundlePrefix is an error api constant for import bundle error messages
const ImportBundlePrefix = "importBundle"
