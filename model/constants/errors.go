package constants

// InvalidPasswordError is returned when an incorrect password is provided to a function that requires the current active password
const InvalidPasswordError = "invalid_password_error"

// PasswordsDoNotMatchError is returned when two passwords do not match
const PasswordsDoNotMatchError = "passwords_do_not_match"
