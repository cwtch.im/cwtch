package testing

import (
	"cwtch.im/cwtch/model/attr"
	"cwtch.im/cwtch/model/constants"
	"cwtch.im/cwtch/peer"
	"cwtch.im/cwtch/protocol/connections"
	"git.openprivacy.ca/openprivacy/log"
	_ "github.com/mutecomm/go-sqlcipher/v4"
	"testing"
	"time"
)

func WaitForConnection(t *testing.T, peer peer.CwtchPeer, addr string, target connections.ConnectionState) {
	peerName, _ := peer.GetScopedZonedAttribute(attr.LocalScope, attr.ProfileZone, constants.Name)
	for {
		log.Infof("%v checking connection...\n", peerName)
		state := peer.GetPeerState(addr)
		log.Infof("Waiting for Peer %v to %v - state: %v\n", peerName, addr, connections.ConnectionStateName[state])
		if state == connections.FAILED {
			t.Fatalf("%v could not connect to %v", peer.GetOnion(), addr)
		}
		if state != target {
			log.Infof("peer %v  %v waiting connect  %v, currently: %v\n", peerName, peer.GetOnion(), addr, connections.ConnectionStateName[state])
			time.Sleep(time.Second * 5)
			continue
		} else {
			log.Infof("peer %v  %v CONNECTED to %v\n", peerName, peer.GetOnion(), addr)
			break
		}
	}
}
