module cwtch.im/cwtch

go 1.23.1

toolchain go1.23.2

require (
	git.openprivacy.ca/cwtch.im/tapir v0.6.0
	git.openprivacy.ca/openprivacy/connectivity v1.11.1
	git.openprivacy.ca/openprivacy/log v1.0.3
	github.com/gtank/ristretto255 v0.1.3-0.20210930101514-6bb39798585c
	github.com/mutecomm/go-sqlcipher/v4 v4.4.2
	github.com/onsi/ginkgo/v2 v2.21.0
	github.com/onsi/gomega v1.35.0
	golang.org/x/crypto v0.28.0
)

require (
	filippo.io/edwards25519 v1.0.0 // indirect
	git.openprivacy.ca/openprivacy/bine v0.0.5 // indirect
	github.com/go-logr/logr v1.4.2 // indirect
	github.com/go-task/slim-sprig/v3 v3.0.0 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/google/pprof v0.0.0-20241029153458-d1b30febd7db // indirect
	github.com/gtank/merlin v0.1.1 // indirect
	github.com/mimoo/StrobeGo v0.0.0-20220103164710-9a04d6ca976b // indirect
	go.etcd.io/bbolt v1.3.6 // indirect
	golang.org/x/net v0.30.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	golang.org/x/text v0.19.0 // indirect
	golang.org/x/tools v0.26.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
